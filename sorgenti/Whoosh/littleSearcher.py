from whoosh.fields import *
import os
from whoosh.index import create_in, open_dir
from whoosh.query import *
from whoosh.qparser import QueryParser


def addDoc(writer, path):
    fileObj = open(path, 'r')
    content = fileObj.read()
    fileObj.close()
    writer.add_document(path=path, content=content)


#index schema
schema = Schema(title=TEXT(stored=True), content=TEXT,
                path=ID(stored=True), tags=KEYWORD, icon=STORED)

if not os.path.exists("index"):
    os.mkdir("index")
    #crea un oggetto store per contenere l'indice
    ix = create_in("index", schema)

else: 
    ix = open_dir("index")
#creo l'index writer per scrivere l'indice
writer = ix.writer()
#aggiungo i vari documenti

docs = os.listdir(".")
for doc in docs:
    if doc.endswith(".txt"):
        addDoc(writer, doc)

writer.commit(optimize=True)
#fase di ricerca
with ix.searcher() as searcher:
    query = input("Cosa vuoi cercare?\n")
    parser = QueryParser("content",ix.schema)
    myquery = parser.parse(query)
    results = searcher.search(myquery)
    if len(results)==0:
        print("Nessun risultato...")
    else: 
        print(results[0])