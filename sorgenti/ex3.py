import nltk
from nltk.corpus import stopwords
import nltk, re, pprint
from nltk import word_tokenize
from urllib import request

url = "https://distrowatch.com/"
response = request.urlopen(url)
raw = response.read().decode('utf8')
tokens = nltk.word_tokenize(raw)


""" nltk.help.upenn_tagset() """

for word, rule in nltk.pos_tag(tokens):
    if(rule == "NN"):
        print(word, rule)
