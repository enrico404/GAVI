import nltk
from nltk.corpus import stopwords
import nltk, re, pprint
from nltk import word_tokenize
from urllib import request

""" url = "https://distrowatch.com/"
response = request.urlopen(url)
raw = response.read().decode('utf8') """
text = "This is a text "
tokens = nltk.word_tokenize(text)

""" print(nltk.pos_tag(tokens)) """
""" nltk.help.upenn_tagset() """
wnl = nltk.WordNetLemmatizer()
for t in tokens:
    if not t in stopwords.words('english'):
        print(wnl.lemmatize(t))
