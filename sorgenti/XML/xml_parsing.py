import xml.sax

class countHandler(xml.sax.handler.ContentHandler):
    def __init__(self):
        #dizionario tags
        self.tags = {}
        self.currentTag = ""
    #chiamato all'inizio di ogni elemento
    def startElement(self, name, attr):
        self.currentTag = name
        if not self.tags.get(name):
            self.tags[name]= []

    #chiamato alla fine di ogni elemento
    def endElement(self, name):
        #quando finisco di leggere un elemento setto il current tag a vuoto
        #altrimenti anche spazi vuoti e andate a capo mantengono lo stesso tag 
        #e vengono inserite all'interno del dizionario
        self.currentTag = ""

    #chiamato alla lettura di ogni carattere
    def characters(self, content):
        if self.currentTag == "brand":
             self.tags["brand"].append(content)
        elif self.currentTag == "model":
            self.tags["model"].append(content)
        elif self.currentTag == "type":
            self.tags["type"].append(content)


parser = xml.sax.make_parser()
handler = countHandler()
parser.setContentHandler(handler)
parser.parse("test.xml")

print(handler.tags)