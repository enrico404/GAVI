def ED(s,t):
    d = {}
    S = len(s)
    T = len(t)
    for i in range(S+1):
        d[i, 0] = i
    for j in range (T+1):
        d[0, j] = j
    for j in range(1,T+1):
        for i in range(1,S+1):
            if s[i-1] == t[j-1]:
                d[i, j] = d[i-1, j-1]
            else:
                d[i, j] = min(d[i-1, j] + 1, d[i, j-1] + 1, d[i-1, j-1] + 1)
    return d[S, T]

addressBook=["Adele","Bryan","James","Jim","John","Richard"]
name = input("inserire nome da cercare: ")
threshold = len(name)
d = {}
print("edit distance vector")
for i in range(len(addressBook)):
    d[i] = ED(name, addressBook[i])
    print(d[i])


min = d[0]
# vado a cercare l'indice dell'elemento più piccolo
for i in range(len(d)):
    if(min > d[i]):
        min = d[i]
        ind = i


print("parola corretta: ", addressBook[ind])