
import nltk

# ####### Tokenizer

print( "\nTOKENIZER")

text = "These are tests"
tokens = nltk.word_tokenize(text)
print( tokens)
# ####### Tagger

print( "\nTAGGER")

tagged=nltk.pos_tag(tokens)

print(tagged)

#print( nltk.help.upenn_tagset())

# ##### Elimination of stopwords

from nltk.corpus import stopwords

tokens2=[]

for t in tokens:
    if not t in stopwords.words('english'):
        tokens2.append(t)

print( "Tokens after elimination of stopwords:")
print( tokens2)

#
# ###### Stemmer
#
print( "\nSTEMMER")

#wordnet lemmatizer

print( "\nWORDNET LEMMATIZER")

wnl = nltk.WordNetLemmatizer()

for t in tokens2:
    print( wnl.lemmatize(t))

#lancaster stemmer

print( "\nLANCASTER STEMMER")

from nltk.stem.lancaster import LancasterStemmer

lancaster = LancasterStemmer()


for t in tokens2:
    print( lancaster.stem(t))

#porter stemmer

print( "\nPORTER STEMMER")

from nltk.stem.porter import PorterStemmer

porter = PorterStemmer()


for t in tokens2:
    print( porter.stem(t))

# ###### WordNet

# from nltk.corpus import wordnet as wn
#
# print("\nDOG SYNSETS")
# print(wn.synsets('dog'))
#
# dog = wn.synset('dog.n.01')
# cat = wn.synset('cat.n.01')
# mouse = wn.synset('mouse.n.01')
# computer = wn.synset('computer.n.01')
# #
# print("\nDOG DEFINITION")
# print(dog.definition())
# #
# print("\nDOG EXAMPLES")
# print(dog.examples())
# #
# print("\nDOG HYPERNYMS")
# print(dog.hypernyms())
# #
# print( "\nPATH SIMILARITY")
# print( "\nDOG <-> CAT")
# print( dog.path_similarity(cat))
# print( "\nDOG <-> COMPUTER")
# print( dog.path_similarity(computer))
# #
# print( "\nWUP SIMILARITY")
# print( "\nDOG <-> CAT")
# print( dog.wup_similarity(cat))
# print( "\nMOUSE <-> CAT")
# print( mouse.wup_similarity(cat))
# print( "\nDOG <-> COMPUTER")
# print( dog.wup_similarity(computer))
# # #
# print( "\nRES SIMILARITY")
# from nltk.corpus import wordnet_ic
# brown_ic = wordnet_ic.ic('ic-brown.dat')
# print( "\nDOG <-> CAT")
# print( dog.res_similarity(cat,brown_ic))
# print( "\nDOG <-> COMPUTER")
# print( dog.res_similarity(computer,brown_ic))
# # #
# print( "\nMORPHY")
# print( wn.morphy('denied', wn.VERB))
# print( wn.morphy('denied', wn.NOUN))
# print( wn.morphy('examples'))
